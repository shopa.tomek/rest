const express = require('express');
const { get } = require('express/lib/response');
const res = require('express/lib/response');
const app = express();





// routes
app.get('/', (req, res)=>{
  res.send('first GET');
});

app.get('/person', (req, res)=>{
  res.send('GET in person')
});

const person = {
  "name": "Adam",
  "surname": "Maly",
  "age": 32
}

app.post('/person',(req, res)=>{
 res.send(person);
});

// listen to the server
port = 3000
app.listen(port, ()=> console.log(`server link: http://localhost:${port}`));
